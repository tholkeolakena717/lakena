CREATE table IF NOT EXISTS authors(
                                      author_id serial4 primary key,
                                      author_name varchar(50) not null ,
    gender varchar(50) not null
    );
CREATE table IF NOT EXISTS books(
                                    book_id serial4 primary key ,
                                    title varchar(255),
    published_date timestamp not null,
    author_id int not null,
    foreign key (author_id) REFERENCES  authors(author_id) On delete cascade on update cascade
    );
Create table if not exists categories(
                                         category_id serial4 primary key ,
                                         category_name varchar(255)
    );
Create table if not exists book_details(
                                           book_detail_id serial4 primary key ,
                                           category_id int not null,
                                           book_id int not null,
                                           foreign key (category_id) REFERENCES  categories(category_id) On delete cascade on update cascade,
    foreign key (book_id) REFERENCES  books(book_id) On delete cascade on update cascade

    );
Create table if not exists users(
                                    id serial4 primary key ,
                                    name varchar(50) not null ,
    email varchar(50) Not null,
    password varchar(50)

    );
select * from authors;
insert into authors(author_name, gender) VALUES ('maiya','Female');
update authors
set author_name='Lolo',gender='Famale'
where author_id=12;
delete from authors where author_id=3;