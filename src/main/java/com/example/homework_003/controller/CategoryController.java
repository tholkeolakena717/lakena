package com.example.homework_003.controller;

import com.example.homework_003.Services.CategoryServices;
import com.example.homework_003.model.ApiRepon;
import com.example.homework_003.model.Author;
import com.example.homework_003.model.Category;
import com.example.homework_003.model.CategoryRequest;
import org.apache.ibatis.annotations.Update;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

@RestController
public class CategoryController {
    private final CategoryServices categoryServices;

    public CategoryController(CategoryServices categoryServices) {
        this.categoryServices = categoryServices;

    }
    @GetMapping("/api/v1/Category")
    public ResponseEntity<?> getAllCategory(){
        List<Category> categories = categoryServices.getAllCategory();
        return ResponseEntity.ok( new ApiRepon<>(
                LocalDateTime.now(),
                200,
                "Seccessfully fetched data!",
                categories
        ));

    }
    @GetMapping("/api/v1/getCategory-id/{id}")
    public ResponseEntity<?> getCategoryById(@PathVariable Integer id){
        Category categoies=categoryServices.getCategoryById(id);
        ApiRepon<Category> respon=ApiRepon.<Category>builder()
                .timeState(LocalDateTime.now())
                .message("Seccessfully fetched data!")
                .payload(categoies)
                .status(200)
                .build();
        return ResponseEntity.ok(respon);

    }
    @PostMapping("/api/v1/Add-category/")
    public ResponseEntity<?> addCategory(@RequestBody CategoryRequest categoryRequest){
        Category categoryes=categoryServices.addCategory(categoryRequest);
        ApiRepon<Category> respon=ApiRepon.<Category>builder()
                .timeState(LocalDateTime.now())
                .message("Seccessfully fetched data!")
                .payload(categoryes)
                .status(200)
                .build();
        return ResponseEntity.ok(respon);

    }
    @PutMapping("/api/v1/update-category/{id}")
    public ResponseEntity<?> updateCategory(@PathVariable Integer id , @RequestBody CategoryRequest categoryRequest){
        Category categoryes=categoryServices.updateCategory(id,categoryRequest);
        ApiRepon<Category> respon=ApiRepon.<Category>builder()
                .timeState(LocalDateTime.now())
                .message("Seccessfully fetched data!")
                .payload(categoryes)
                .status(200)
                .build();
        return ResponseEntity.ok(respon);

    }
    @DeleteMapping ("/api/v1/Delete-category/{id}")
    public ResponseEntity<?> deleteCategory(@PathVariable Integer id ){
        Category categoryes=categoryServices.deleteCategory(id);
        ApiRepon<Category> respon=ApiRepon.<Category>builder()
                .timeState(LocalDateTime.now())
                .message("Delete fetched data!")
                .payload(categoryes)
                .status(200)
                .build();
        return ResponseEntity.ok(respon);

    }


    }

