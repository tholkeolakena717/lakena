package com.example.homework_003.controller;

import com.example.homework_003.Services.BookServices;
import com.example.homework_003.model.ApiRepon;
import com.example.homework_003.model.Books;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.List;

@RestController
public class BookController {
    private final BookServices bookServices;

    public BookController(BookServices bookServices) {
        this.bookServices = bookServices;
    }

    @GetMapping("/api/v1/book")
    public ResponseEntity<?> getAllBook(){
        List<Books> books = bookServices.getAllBook();
        return ResponseEntity.ok( new ApiRepon<List<Books>>(
                LocalDateTime.now(),
                200,
                "Seccessfully fetched data!",
                books
        ));




    }
}
