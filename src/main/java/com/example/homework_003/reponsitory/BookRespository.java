package com.example.homework_003.reponsitory;

import com.example.homework_003.model.Books;
import org.apache.ibatis.annotations.*;

import java.util.List;
@Mapper
public interface BookRespository {
//    @Results(id = "MapBook",value = {
//            @Result(property="bookId",column ="book_id"),
//            @Result(property="title",column ="title"),
//            @Result(property="pushlisedDate",column ="published_date"),
//            @Result(property="pushlisedDate",column ="published_date"),
//
//    })


    @Select("""
            select * from books
            
            """)
            @Result(property = "author",column = "author_id",
            one=@One(select="com.example.homework_003.reponsitory.AuthorReponsitory.getAuthorById")),



    List<Books> getAllBook();
}
