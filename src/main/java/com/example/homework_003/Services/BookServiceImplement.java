package com.example.homework_003.Services;

import com.example.homework_003.model.Books;
import com.example.homework_003.reponsitory.BookRespository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookServiceImplement implements  BookServices{

    private final BookRespository  bookRespository;

    public BookServiceImplement(BookRespository bookRespository) {
        this.bookRespository = bookRespository;
    }

    @Override
    public List<Books> getAllBook() {
        return bookRespository.getAllBook();
    }
}
