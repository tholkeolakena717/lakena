package com.example.homework_003.Services;

import com.example.homework_003.model.Category;
import com.example.homework_003.model.CategoryRequest;

import java.util.List;

public interface CategoryServices {
    List<Category> getAllCategory();

    Category getCategoryById(Integer id);


    Category addCategory(CategoryRequest categoryRequest);

    Category updateCategory(Integer id, CategoryRequest categoryRequest);

    Category deleteCategory(Integer id);
}
