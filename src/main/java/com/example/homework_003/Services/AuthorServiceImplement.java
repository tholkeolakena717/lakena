package com.example.homework_003.Services;

import com.example.homework_003.model.Author;
import com.example.homework_003.model.AuthorRequest;
import com.example.homework_003.reponsitory.AuthorReponsitory;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AuthorServiceImplement implements   AuthorServices{
    private  final AuthorReponsitory  authorReponsitory;

    public AuthorServiceImplement(AuthorReponsitory authorReponsitory) {
        this.authorReponsitory = authorReponsitory;
    }

    @Override
    public List<Author> getAllAuthor() {
        return authorReponsitory.getAllAuthor();
    }

    @Override
    public Author getAuthorById(Integer id) {
        return  authorReponsitory. getAuthorById(id) ;
    }

    @Override
    public Author addAuthor(AuthorRequest authorRequest) {
        return authorReponsitory.addAuthor(authorRequest);
    }

    @Override
    public Author UpdateAuthor(Integer id, AuthorRequest authorRequest) {
         authorReponsitory.UpdateAuthor(id,authorRequest) ;
        return  authorReponsitory. getAuthorById(id);
    }

    @Override
    public Author DeleteAuthorById(Integer id) {
       authorReponsitory.DeleteAuthorById(id);
        return  authorReponsitory. getAuthorById(id);
    }
}
