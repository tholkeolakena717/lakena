package com.example.homework_003.Services;

import com.example.homework_003.model.Author;
import com.example.homework_003.model.AuthorRequest;

import java.util.List;


public interface AuthorServices {

    List<Author> getAllAuthor();

    Author getAuthorById(Integer id);

    Author addAuthor(AuthorRequest author);

    Author UpdateAuthor(Integer id, AuthorRequest authorRequest);

    Author DeleteAuthorById(Integer id);
}
