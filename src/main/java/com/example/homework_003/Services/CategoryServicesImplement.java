package com.example.homework_003.Services;

import com.example.homework_003.model.Category;
import com.example.homework_003.model.CategoryRequest;
import com.example.homework_003.reponsitory.CategoryResponsitory;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryServicesImplement   implements   CategoryServices {
    private final CategoryResponsitory  categoryResponsitory;

    public CategoryServicesImplement(CategoryResponsitory categoryResponsitory) {
        this.categoryResponsitory = categoryResponsitory;
    }

    @Override
    public List<Category> getAllCategory() {
        return categoryResponsitory.getAllCategory();
    }

    @Override
    public Category getCategoryById(Integer id) {
        return categoryResponsitory.getCategoryById(id);
    }

    @Override
    public Category addCategory(CategoryRequest categoryRequest) {
        return categoryResponsitory.addCategory(categoryRequest);
    }

    @Override
    public Category updateCategory(Integer id, CategoryRequest categoryRequest) {
         categoryResponsitory.updateCategory(id,categoryRequest);
        return categoryResponsitory.getCategoryById(id);
    }

    @Override
    public Category deleteCategory(Integer id) {
        return categoryResponsitory.deleteCategory(id);
    }


}
